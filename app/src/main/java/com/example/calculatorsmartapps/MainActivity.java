package com.example.calculatorsmartapps;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText Panjang, Lebar;
    Button Hitung;
    TextView Hasil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Panjang = (EditText) findViewById(R.id.EditPanjang);
        Lebar = (EditText) findViewById(R.id.EditLebar);
        Hasil = (TextView) findViewById(R.id.TextHasil);
        Hitung = (Button) findViewById(R.id.buttonHitung);
        Hitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String p = Panjang.getText().toString();
                String l = Lebar.getText().toString();

                if (Panjang.length() == 0 ){
                    Toast.makeText(MainActivity.this, "Panjang Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                }else if (Lebar.length() == 0){
                    Toast.makeText(MainActivity.this, "Lebar Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                } else {
                    int panjang = Integer.parseInt(p);
                    int lebar = Integer.parseInt(l);
                    int luas = panjang * lebar;
                    Hasil.setText(String.valueOf(luas));
                }


            }
        });
    }
}